var gulp = require('gulp');
var concat = require('gulp-concat');

gulp.task('build', function() {
    var scripts = ['./src/prefix.js',
                   './src/util.js',
                   './src/spritz.js',
                   './src/suffix.js'];
    return gulp.src(scripts)
        .pipe(concat('spritz.js'))
        .pipe(gulp.dest('./dist/'));
});
