var N = 256;
var N_OVER_TWO_FLOOR =  Math.floor(N / 2);
var TWO_N = 2 * N;

var spritz = {};

spritz._util = util;

spritz._initializeState = function _initializeState() {
    var state = {};

    state.i = 0;
    state.j = 0;
    state.k = 0;
    state.z = 0;
    state.a = 0;
    state.w = 1;

    state.S = [];
    for (var i = 0; i < N; i++) {
        state.S[i] = i;
    }
    return state;
};

spritz._absorb = function _absorb(I, state){
    var v;
    /*
      basic validation of I
      consider bounds-checking and deep-checking of each N-value such that they are in the valid range (0 <= x < N)
    */
    if(!(Array.isArray(I) && I.length > 0)){
        return false;
    }

    for(v = 0; v < I.length; v++){
        spritz._absorbByte(I[v], state);
    }
    return true;
};

spritz._absorbByte = function _absorbByte(b, state) {
    spritz._absorbNibble(util.low(b), state);
    spritz._absorbNibble(util.high(b), state);
};

spritz._absorbNibble = function _absorbNibble(x, state) {
    if (state.a === N_OVER_TWO_FLOOR){
        spritz._shuffle(state);
    }

    util.swap(state.S, state.a, util.madd(N_OVER_TWO_FLOOR, x));
    state.a = util.madd(state.a, 1);
};

spritz._absorbStop = function _absorbStop(state){
    if(state.a === N_OVER_TWO_FLOOR){
        spritz._shuffle(state);
    }
    state.a = util.madd(state.a, 1);
};

spritz._shuffle = function _shuffle(state){
        spritz._whip(TWO_N, state);
        spritz._crush(state);
        spritz._whip(TWO_N, state);
        spritz._crush(state);
        spritz._whip(TWO_N, state);
        state.a = 0;
};

spritz._whip = function _whip(r, state) {
    for (var i = 0; i < r; i++) {
        spritz._update(state);
    }
    do {
        state.w = util.madd(state.w, 1);
    } while (util.gcd(state.w, N) !== 1);
    // NB. a simple-case assumption is that if N is a power of 2 then one could instead use:
    // w = madd(w, 2);
};

spritz._crush = function _crush(state) {
    var index;
    for (var i = 0; i < N_OVER_TWO_FLOOR; i++) {
        index = N - i - 1;
        if (state.S[i] > state.S[index]) {
            util.swap(state.S, i, index);
        }
    }
};

spritz._update = function _update(state) {  //done
    state.i = util.madd(state.i, state.w);
    state.j = util.madd(state.k, state.S[util.madd(state.j, state.S[state.i])]);
    state.k = util.madd(state.i + state.k, state.S[state.j]);
    util.swap(state.S, state.i, state.j);
};

spritz._squeeze = function _squeeze(r, state) {
    if (state.a > 0) {
        spritz._shuffle(state);
    }

    var P = [];
    for (var i = 0; i < r; i++) {
        P[i] = spritz._drip(state);
    }
    return P;
};

spritz._drip = function _drip(state){
    if (state.a > 0) {
        spritz._shuffle(state);
    }
    spritz._update(state);
    return spritz._output(state);
};

spritz._output = function _output(state) {
    var idx = util.madd(state.z, state.k);
    idx = util.madd(state.i, state.S[idx]);
    idx = util.madd(state.j, state.S[idx]);
    state.z = state.S[idx];
    return state.z;
};

spritz._keySetup = function _keySetup(K) {
   var state = spritz._initializeState();
   spritz._absorb(K, state);
   return state;
};

spritz.stringToUtf8ByteArray = function stringToUtf8ByteArray(utf8Array){
    var utf8 = unescape(encodeURIComponent(utf8Array));

    var arr = [];
    for (var i = 0; i < utf8.length; i++) {
        arr.push(utf8.charCodeAt(i));
    }
    return arr;
};

spritz.utf8ByteArrayToString = function utf8ByteArrayToString(utf8Array){
    var ret = "";
    for(var i = 0; i < utf8Array.length; i++){
        ret += String.fromCharCode(utf8Array[i]);
    }
    return decodeURIComponent(escape(ret));
};

spritz.hash = function hash(M, r){
    if(r > 255){
        throw "cannot hash into more than 255 bytes";
    }
    if(typeof M == 'string'){
        return spritz.hash(spritz.stringToUtf8ByteArray(M), r);
    }
    var state = spritz._initializeState();
    spritz._absorb(M, state);
    spritz._absorbStop(state);
    spritz._absorb([r & 0xff], state);  // NB. restricted(!) to 255-byte hashes
    return spritz._squeeze(r, state);
};

spritz.encrypt = function encrypt(K, M) {

    var C = [], stream , i;

    var state = spritz._keySetup(K);
    stream = spritz._squeeze(M.length, state);
    for (i = 0; i < M.length; i++) {
        C[i] = util.madd(M[i], stream[i]);
    }
    return C;
};

spritz.decrypt = function decrypt(K, C) {
    var M = [] , stream , i;
    var state = spritz._keySetup(K);
    stream = spritz._squeeze(C.length, state);
    for (i = 0; i < C.length; i++) {
        M[i] = util.msub(C[i], stream[i]);
    }
    return M;
};
