// Export spritz as lodash 4.5.0.

var objectTypes = {
   'function': true,
   'object': true
};

function checkGlobal(value) {
    return (value && value.Object === Object) ? value : null;
}

var freeWindow = checkGlobal(objectTypes[typeof window] && window);
var freeSelf = checkGlobal(objectTypes[typeof self] && self);
var freeGlobal = checkGlobal(freeExports && freeModule && typeof global == 'object' && global);
var freeModule = (objectTypes[typeof module] && module && !module.nodeType) ? module : undefined;
var freeExports = (objectTypes[typeof exports] && exports && !exports.nodeType) ? exports : undefined;
var moduleExports = (freeModule && freeModule.exports === freeExports) ? freeExports: undefined;
var thisGlobal = checkGlobal(objectTypes[typeof this] && this);


var root = freeGlobal || ((freeWindow !== (thisGlobal && thisGlobal.window)) && freeWindow) || freeSelf || thisGlobal || Function('return this')();


// Expose lodash on the free variable `window` or `self` when available. This
// prevents errors in cases where lodash is loaded by a script tag in the presence
// of an AMD loader. See http://requirejs.org/docs/errors.html#mismatch for more details.
(freeWindow || freeSelf || {}).spritz = spritz;

// Some AMD build optimizers like r.js check for condition patterns like the following:
if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
  // Define as an anonymous module so, through path mapping, it can be
  // referenced as the "underscore" module.
  define(function() {
    return spritz;
  });
}
// Check for `exports` after `define` in case a build optimizer adds an `exports` object.
else if (freeExports && freeModule) {
  // Export for Node.js.
  if (moduleExports) {
      freeModule.exports = spritz;
  }
  // Export for CommonJS support.
  freeExports.spritz = spritz;
}
else {
  // Export to the global object.
  root.spritz = spritz;
}


}.call(this)); // close the prefix
