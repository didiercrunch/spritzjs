var util = {};
util.low = function(byte) {
    return byte & 0xf;
};


util.high = function(byte) {
    return byte >>> 4 & 0xf;
};

util.madd = function(a, b) { // TODO: rename to madd256
   return (a + b) % N;
   // return (a + b) & 0xff;      // when N = 256, 0xff = N - 1
 };


util.msub = function(a, b) {// TODO: rename to msub256
   return util.madd(N, a - b);
};

util.swap = function(lst, p1, p2) {
    var tmp = lst[p1];
    lst[p1] = lst[p2];
    lst[p2] = tmp;
};

util.gcd = function(a, b){
    var t;
    while (b !== 0) {
      t = b;
      b = a % b;
      a = t;
    }
    return a;
};
