var util = require("../dist/spritz.js")._util;
var spritz = require("../dist/spritz.js");
var assert = require('assert');

describe('utility functions', function() {

    describe('the low function', function(){
        it('should get the low nibble of the input', function () {
            assert.equal(util.low(3), 3);
            assert.equal(util.low(0), 0);
            assert.equal(util.low(255), 15);
        });
    });

});

describe('the string to array convertion', function(){
    it('should be able to convert string to utf-8 array', function(){
        var utf8String = "maison-дом-տուն-სახლი";
        var expt = [109,97,105,115,111,110,45,208,180,208,190,208,188,45,213,191,213,184,214,130,213,182,45,225,131,161,225,131,144,225,131,174,225,131,154,225,131,152];
        assert.deepEqual(spritz.stringToUtf8ByteArray(utf8String), expt);
    });

    it('should be able to convert utf-8 array to string', function(){
        var utf8Array = [109,97,105,115,111,110,45,208,180,208,190,208,188,45,213,191,213,184,214,130,213,182,45,225,131,161,225,131,144,225,131,174,225,131,154,225,131,152];
        var expt = "maison-дом-տուն-სახლი";
        assert.deepEqual(spritz.utf8ByteArrayToString(utf8Array), expt);
    });
});


describe('the hash function', function(){
    it('should be able to hash an array', function(){
        var M = [65, 66, 67];
        var r = 32;
        var expt = [2,143,162,180,139,147,74,24,98,184,105,16,81,58,71,103,124,28,45,149,236,62,117,112,120,111,28,50,139,189,74,71];
        assert.deepEqual(spritz.hash(M, r), expt);
    });

    it('should be able to hash a string', function(){
        var M = 'ABC';
        var r = 32;
        var expt = [2,143,162,180,139,147,74,24,98,184,105,16,81,58,71,103,124,28,45,149,236,62,117,112,120,111,28,50,139,189,74,71];
        assert.deepEqual(spritz.hash(M, r), expt);
    });

    it('should be able to hash into big outputs', function(){
        var M = 'key';
        var r = 128;
        var expt = [161,146,113,209,92,234,84,76,1,178];
        assert.deepEqual(spritz.hash(M, r).splice(0, 10), expt);

        r = 196;
        expt = [70,186,211,241,183,79,116,6,36,178,244,78,18,13,72,203,248,160,165,201,59];
        assert.deepEqual(spritz.hash(M, r).splice(0, expt.length), expt);


    });

    it('should be able to hash a big chunck of text', function(){
        var message = "";
        for(var i = 0; i < 1000; i++){
            message += "key";
        }
        r = 196;
        expt = [53,4,194,202,251,2,132,3,208,1,176,128,17,208,30,26,47,86,68,211,170];
        assert.deepEqual(spritz.hash(message, r).splice(0, expt.length), expt);

        r = 255;
        expt = [148,179,139,174,155,47,98,51,153,231,105,247,132,230,146];
        assert.deepEqual(spritz.hash(message, r).splice(0, expt.length), expt);
    });
});

describe('the encrypt/decrypt function', function(){
    it('should be encrypt/decrypt a byte array', function(){
        var stringMessage = "maison-дом-տուն-სახლი";
        var byteArrMessage = spritz.stringToUtf8ByteArray(stringMessage);
        var key = [12, 99, 200, 76, 162, 87, 729, 272, 272, 282, 282];
        var cypher = spritz.encrypt(key, byteArrMessage);
        var decryptedMessage = spritz.decrypt(key, cypher);
        assert.deepEqual(decryptedMessage, byteArrMessage);
    });

    xit('should encrypt correctly the message', function(){
        var stringMessage = "maison-дом-տուն-სახლი";
        var byteArrMessage = spritz.stringToUtf8ByteArray(stringMessage);
        var key = [12, 55, 12, 12, 87, 89, 81, 17, 27, 89];
        var cypher = spritz.encrypt(key, byteArrMessage);
        var expt = [125,26,182,7,75,36,178,253,82,46,91,21,218,16,127,41,203,45,90,129,129,47,171,68,221,112,210,6,99,250,91,111,225,27,89,100,189,9];
        assert.deepEqual(cypher, expt);
    });

});
